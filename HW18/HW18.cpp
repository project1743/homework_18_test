﻿// HW18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <utility>
#include <algorithm>
void bubbleSort(int size[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)

		// Last i elements are already
		// in place
		for (j = 0; j < n - i - 1; j++)
			if (size[j] > size[j + 1])
				std::swap(size[j], size[j + 1]);
}

// Function to print an array
void printArray(int size[], int Size)
{
	int i;
	for (i = 0; i < Size; i++)
		std::cout << size[i] << " ";
	std::cout << std::endl;
}

class AddPlayer
{
private:
	char name[256];
	int score = 0;
public:
	int GetScore()
	{
		return score;
	}
	
	void Enter()
	{
		std::cout << "Enter your name:" << '\n';
		std::cin >> name;
		std::cout << "Enter score:";
		std::cin >> score;
		
	}
	
	void Show()
	{
		std::cout << name << " " << score << '\n';
	}
	
	
};


int main()
{
	int size;
	std::cout << "Enter number of players: ";
	std::cin >> size;

	AddPlayer* Array = new AddPlayer[size];

	
	for (int x = 0; x < size; ++x)
	{
		Array[x].Enter();
		Array[x + 1].GetScore();
	}
	std::cout << "\n";


	int N = sizeof(size) / sizeof(size + 1);
	bubbleSort(&size, N);
	std::cout << "Sorted array: \n";
	printArray(&size, N);
	return 0;
	
			
	delete[] Array;
	Array = nullptr;

}






// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
